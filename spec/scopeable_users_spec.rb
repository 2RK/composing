require "spec_helper"
require_relative "../example_data/users"

describe ScopeableUsers do
  it "works like an active record object" do
    expect(ScopeableUsers.new(@users).children.with_birthdate).to match_array([

                                                                                {
                                                                                  id: 1,
                                                                                  name: "Ally",
                                                                                  email: "ally.ridge@google.com",
                                                                                  adult: false,
                                                                                  birthdate: "04-08-2015"
                                                                                },
                                                                                {
                                                                                  id: 3,
                                                                                  name: "Josie",
                                                                                  email: "josie.jarrad@google.com",
                                                                                  adult: false,
                                                                                  birthdate: "16-05-85"
                                                                                }
                                                                              ])

  end
end
