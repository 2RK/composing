# Composition

## Instructions

You're gonna make a hash behave like an ActiveRecord model with scopes, so we can call `foo.bar.baz` using composition and some of the ruby standard library

I have a test that I need you to get passing, outside of that test are some hashes which will be used as test data and for you to play around with

You'll need to use `SimpleDelegator`, `tap`, and `delete_if` for this to work.

Assuming you use Git, when you’re ready to submit your solution, please use `git bundle` to package up a copy of your repository (with complete commit history) as a single file and send it to us as an email attachment.

```
git bundle create composition.bundle master
```




