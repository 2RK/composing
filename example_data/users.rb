@users = [
  {
    id: 1,
    name: "Ally",
    email: "ally.ridge@google.com",
    adult: false,
    birthdate: "04-08-2015"
  },
  {
    id: 2,
    name: "Chloe",
    email: "chloe.ridge@google.com",
    adult: false,
    birthdate: nil
  },
  {
    id: 3,
    name: "Josie",
    email: "josie.jarrad@google.com",
    adult: false,
    birthdate: "16-05-85"
  },
  {
    id: 3,
    name: "Tom",
    email: "tom.ridge@google.com",
    adult: true,
    birthdate: nil
  },
  {
    id: 4,
    name: "Mitchell",
    email: "mitchell.buckley@google.com",
    adult: true,
    birthdate: "28-05-1985"
  }
]
